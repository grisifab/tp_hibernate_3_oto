package org.eclipse.main;

import java.util.List;

import org.eclipse.model.Adresse;
import org.eclipse.model.Personne;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	Adresse adresse = new Adresse();
    	adresse.setRue("Lyon"); 
    	adresse.setCodePostal("13015"); 
    	adresse.setVille("Marseille"); 
    	
    	Personne personne = new Personne();
    	personne.setAdresse(adresse); 
    	personne.setNom("Ego"); 
    	personne.setPrenom("Paul"); 
    	   
        System.out.println("jusqu'ici tout va bien 1");
        Configuration configuration = new Configuration().configure();
        System.out.println("jusqu'ici tout va bien 2");
        SessionFactory sessionFactory = configuration.buildSessionFactory();
        System.out.println("jusqu'ici tout va bien 3");
        Session session = sessionFactory.openSession();
        System.out.println("jusqu'ici tout va bien 4");
        Transaction transaction = session.beginTransaction();
        System.out.println("jusqu'ici tout va bien 5");
        session.persist(personne);
        //session.persist(adresse);
        transaction.commit(); 
        session.close(); 
        sessionFactory.close();
        System.out.println("jusqu'ici tout va bien 9");
    }
}
